<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class PropertySearch
{

    /**
     * @var int|null
     */
    private $maxPrice;

    /**
     * @var int|null
     * @Assert\Range(min=10, max=1500)
     */
    private $minSurfaceArea;

    /**
     * @var ArrayCollection
     */
    private $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(int $maxPrice): PropertySearch
    {
        $this->maxPrice = $maxPrice;
        return $this;
    }

    public function getMinSurfaceArea(): ?int
    {
        return $this->minSurfaceArea;
    }

    public function setMinSurfaceArea(int $minSurfaceArea): PropertySearch
    {
        $this->minSurfaceArea = $minSurfaceArea;
        return $this;
    }

    public function getOptions(): ?ArrayCollection
    {
        return $this->options;
    }

    public function setOptions(ArrayCollection $options): PropertySearch
    {
        $this->options = $options;
        return $this;
    }

}
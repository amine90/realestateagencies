<?php
namespace App\Controller\Admin;

use App\Entity\Option;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Property;
use App\Form\ProperyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\component\HttpFoundation\Response;

/**
 * @Route("/admin")
 */
class AdminProperyController extends AbstractController {


    private $repository;

    private $em;

    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;   
        $this->em = $em;   
    }

    /**
     * @Route("/", name="admin.property.index")
     * @return \Symfony\component\HttpFoundation\Response
     */
    public function index()
    {
        $properties = $this->repository->findAll();
        return $this->render('admin/property/index.html.twig', compact ('properties'));
    }

    
    /**
     * @Route("/property/create", name="admin.property.create")
     * @param Request $request
     * @return \Symfony\component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $property = new Property();
        $form = $this->createForm(ProperyType::class, $property);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

            if ($form->isSubmitted() && $form->isValid()) {
                $this->em->persist($property);
                $this->em->flush();
                $this->addFlash('success', "Proprety successfully created");
                return $this->redirectToRoute('admin.property.index');
            }
        }

        return $this->render('admin/property/new.html.twig', /* compact ($property) OR */ [
            'property' => $property,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/property/{id}", name="admin.property.edit",  methods="GET|POST")
     * @param Property $property
     * @param Request $request
     * @return \Symfony\component\HttpFoundation\Response
     */
    public function edit(Property $property, Request $request)
    {
        $form = $this->createForm(ProperyType::class, $property);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            // $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                $this->em->flush();
                $this->addFlash('success', "Proprety successfully updated");
                return $this->redirectToRoute('admin.property.index');
            }
        }

        return $this->render('admin/property/edit.html.twig', /* compact ($property) OR */ [
            'property' => $property,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/property/{id}", name="admin.property.delete",  methods="DELETE")
     * @param Property $property
     * @param Request $request
     * @return \Symfony\component\HttpFoundation\Response
     */
    public function delete(Property $property, Request $request)
    {
        if($this->isCsrfTokenValid('delete'.$property->getId(), $request->get('_token'))){
            $this->em->remove($property);
            $this->em->flush();
            $this->addFlash('success', "Proprety successfully deleted");
        }
        return $this->redirectToRoute('admin.property.index');
;    }

}